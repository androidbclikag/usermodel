package com.example.usermodel

class User(
    val name: String,
    val lastName: String,
    val age: Int,
    val language: List<String>
)

val person = User("Lika", "Glonti", 19, arrayListOf("python", "kotlin"))
