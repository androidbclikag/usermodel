package com.example.usermodel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getPerson(person)
    }

    fun getPerson(User: User) {
        nameTextView.text = User.name
        lastNameTextView.text = User.lastName
        ageTextView.text = User.age.toString()
        var lang: String = ""
        for (element in User.language)
            lang += "$element "
        languageTextView.text = lang
    }

}
